import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgbTypeaheadSelectItemEvent } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, map, switchMap } from 'rxjs/operators';
import { GeoLocation, GeoLocationsResponse } from '../../interfaces/geo-location.interface';
import { GeoLocationService } from '../../services/geo-location.service';
import { NotificationsService } from '../../services/notifications.service';


@Component({
  selector: 'app-search-location',
  templateUrl: './search-location.component.html',
  styleUrls: ['./search-location.component.scss']
})
export class SearchLocationComponent implements OnInit {

  @Output() selected = new EventEmitter<GeoLocation>();
  @Output() enter = new EventEmitter<GeoLocation>()

  selectedItem: GeoLocation;

  constructor(
    private geoSearch: GeoLocationService,
    private notificationsService: NotificationsService
  ) { }

  ngOnInit(): void {
  }

  formatter(result: GeoLocation) {
    if (result) {
      return result.displayName;
    }
  }

  keyupEnter() {
    if (this.selectedItem) {
      this.enter.emit(this.selectedItem);
    }
  }

  select(selected: NgbTypeaheadSelectItemEvent) {
    this.selectedItem = selected.item;
    this.selected.emit(selected.item);
  }

  search = (text: Observable<string>) =>
    text.pipe(
      debounceTime(1000),
      distinctUntilChanged(),
      switchMap(term => {
        if (term.length === 0) {
          this.selected.emit();
          this.resetSelectedItem();
          return [];
        }
        if (term.length < 3) {
          this.resetSelectedItem();
          return [];
        }
        return this.geoSearch.search(term)
          .pipe(
            catchError(() => {
              this.resetSelectedItem();
              this.notificationsService.warning(this.geoSearch.errorMsg, "GeoServer problem")
              return of(<GeoLocationsResponse>{ locations: [] });
            }),
            map(v => v.locations)
          )
      }
      )
    );

  private resetSelectedItem() {
    this.selectedItem = null;
  }

}
