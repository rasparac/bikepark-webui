import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { Subscription } from 'rxjs';
import { CreateUserComponent } from '../../../modules/users/components/create-user/create-user.component';
import { EditProfileComponent } from '../../../modules/users/components/edit-profile/edit-profile.component';
import { User } from '../../../modules/users/interfaces/user.interface';
import { UsersService } from '../../../modules/users/services/users.service';
import { AuthUser } from '../../interfaces/auth-user.interface';
import { AuthService } from '../../services/auth.service';
import { JwtService } from '../../services/jwt.service';
import { NotificationsService } from '../../services/notifications.service';
import { SpinnerService } from '../../services/spinner.service';
import { LoginComponent } from '../login/login.component';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  collapsed = false;
  isLoggedIn = false;
  isLoggedInSubscription: Subscription;
  user: User;
  authUser: AuthUser;

  constructor(
    private authService: AuthService,
    private modalService: NgbModal,
    private usersService: UsersService,
    private jwtService: JwtService,
    private cookieService: CookieService,
    private spinnerService: SpinnerService,
    private notificationsService: NotificationsService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.isLoggedInSubscription = this.authService.isLoggedIn().subscribe(v => {
      if (v != null) {
        this.isLoggedIn = v;
      }
    })
    this.authUser = this.authService.getUser();
    if (this.authUser) {
      this.isLoggedIn = true;
      this.getAndSetUser();
    }
    this.authService.getUserObservable().subscribe(
      r => {
        this.authUser = r
      }
    )
  }

  ngOnDestroy(): void {
    this.isLoggedInSubscription.unsubscribe()
  }

  toggleNavbar(): void {
    this.collapsed = !this.collapsed;
  }

  updateProfile(): void {
    const m = this.modalService.open(EditProfileComponent, {
      size: 'lg',
      centered: true
    });
    m.componentInstance.name = `users/${this.authUser.userId}`;
    m.result.then((value: User) => {
      if (value) {
        this.authUser.username = value.username;
        this.user = value;
        this.notificationsService.success("Profile has been successfully updated.");
      }
    }, _ => { });
  }

  createAccount(): void {
    this.modalService.open(CreateUserComponent, {
      centered: true
    });
  }

  logout(): void {
    this.authService.logout();
    this.router.navigate(["/"]);
  }

  goToUsers(): void {
    this.router.navigate(["users"]);
  }

  goToMyLocations(): void {
    this.router.navigate(["my-bikeparkings"])
  }

  goToBikeThefts(): void {
    this.router.navigate(["my-bikethefts"])
  }

  login(): void {
    const m = this.modalService.open(LoginComponent, {
      size: 'sm',
      centered: true
    });
    m.result.then(token => {
      if (token) {
        try {
          const decodedToken = this.jwtService.parse(token);
          this.cookieService.set('token', token);
          const u = <AuthUser>{
            userId:  decodedToken.user_id,
            username: decodedToken.username
          };
          this.usersService.one(`users/${u.userId}`).subscribe(
            user => {
              this.user = user;
              u.userLocation = this.user.userLocation;
              this.authService.setUser(u);
              this.authService.setLogin(true);
            },
            (err: HttpErrorResponse) => {
              this.notificationsService.fromError(err, "Login problem");
            }
          )
        } catch {
          this.notificationsService.error("An error occurred while parsing token.")
        }
      }
    }, _ => { });
  }

  private getAndSetUser(): void {
    this.spinnerService.spin();
    this.usersService.one(`users/${this.authUser.userId}`).subscribe(
      user => {
        this.user = user;
        this.authUser.userLocation = this.user.userLocation
        this.authService.setUser(this.authUser)
        this.authService.setLogin(true);
        this.spinnerService.stop();
      },
      (error: HttpErrorResponse) => {
        this.notificationsService.fromError(error);
        this.spinnerService.stop();
      }
    );
  }
}
