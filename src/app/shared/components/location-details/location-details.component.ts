import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Location } from '../../../../app/modules/locations/interfaces/location.inteface';
import { AuthUser } from '../../interfaces/auth-user.interface';
import { Review } from '../../interfaces/reviews.interface';
import { AuthService } from '../../services/auth.service';
import { NotificationsService } from '../../services/notifications.service';
import { ReviewsFilter, ReviewsService } from '../../services/reviews.service';
import { SpinnerService } from '../../services/spinner.service';

@Component({
  selector: 'app-location-details',
  templateUrl: './location-details.component.html',
  styleUrls: ['./location-details.component.scss']
})
export class LocationDetailsComponent implements OnInit {
  
  reviewForm: FormGroup;
  active = 1;
  user: AuthUser = null;
  currentRate = 0;
  isLoggedIn = false;
  reviews: Review[] = []

  @Input() location: Location;

  constructor(
    public activeModal: NgbActiveModal,
    private reviewsService: ReviewsService,
    private authService: AuthService,
    private spinnerService: SpinnerService,
    private notificationsService: NotificationsService
  ) {
    this.reviewForm = new FormGroup({
      comment: new FormControl(null, [Validators.required]),
      rating: new FormControl(null, [Validators.required])
    })
  }

  ngOnInit(): void {
    this.user = this.authService.getUser()

    this.authService.isLoggedIn().subscribe(is => {
      this.isLoggedIn = is;
    });

    const placeIdSplited = this.location.name.split("/")
    if (placeIdSplited.length != 2) {
      return
    }

    this.fetchReviews()
  }

  save(): void {
    const placeIdSplited = this.location.name.split("/")
    if (placeIdSplited.length != 2) {
      return
    }
    const review = <Review>{
      placeId: placeIdSplited[1],
      comment: this.reviewForm.get('comment').value,
      rating: this.reviewForm.get('rating').value,
    }

    this.spinnerService.spin()

    this.reviewsService.post(review).toPromise()
    .then(
      () => {
        this.reviewForm = new FormGroup({
          comment: new FormControl(null, [Validators.required]),
          rating: new FormControl(null, [Validators.required])
        })

        this.fetchReviews()
      },
      (err: HttpErrorResponse) => {
        this.notificationsService.fromError(err, "Unable to create review")
      }
    ).finally(()=> {
      this.spinnerService.stop()
    })
  }

  private fetchReviews() {
    const placeIdSplited = this.location.name.split("/")
    if (placeIdSplited.length != 2) {
      return
    }

    this.spinnerService.spin()

    this.reviewsService.list(<ReviewsFilter>{
      "filter.place_id": placeIdSplited[1],
    })
    .toPromise()
    .then(
      (resp) => {
        this.reviews = resp.reviews
      },
      (err: HttpErrorResponse) => {
        this.notificationsService.fromError(err, "Unable to fetch reviews")
      }
    )
    .finally(() => {
      this.spinnerService.stop()
    })
  }

}
