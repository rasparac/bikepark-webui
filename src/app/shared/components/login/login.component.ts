import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from '../../services/auth.service';
import { NotificationsService } from '../../services/notifications.service';
import { SpinnerService } from '../../services/spinner.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  modalRef: NgbModalRef
  loginForm: FormGroup

  constructor(
    public activeModal: NgbActiveModal,
    private authService: AuthService,
    private spinnerService: SpinnerService,
    private notificationsService: NotificationsService
  ) {
    this.loginForm = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    });
  }

  ngOnInit(): void {
  }

  close(): void {
    this.activeModal.close();
  }

  login(): void {
    this.spinnerService.spin();
    this.authService.login(this.loginForm.value).subscribe(
      r => {
        this.spinnerService.stop();
        this.activeModal.close(r.token);
      },
      (err: HttpErrorResponse) => {
        this.notificationsService.fromError(err);
        this.spinnerService.stop();
      }
    )
  }

}
