import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationComponent } from '../components/confirmation/confirmation.component';

export interface ConfirmConfig {
  title?: string
  message?: string
  okBtnText?: string
  cancelBtnText?: string
}

@Injectable({
  providedIn: 'root'
})
export class ConfirmationService {

  constructor(
    private modalService: NgbModal
  ) { }

  confirm(c?: ConfirmConfig): Promise<boolean> {
    const instance = this.modalService.open(ConfirmationComponent, {
      size: 'sm',
      centered: true
    })
    const conf = Object.assign(<ConfirmConfig>{
      cancelBtnText: 'Cancel',
      message: '',
      okBtnText: 'Yes',
      title: 'Are you sure?'
    }, c)
    instance.componentInstance.title = conf.title;
    instance.componentInstance.message = conf.message;
    instance.componentInstance.okBtnText = conf.okBtnText;
    instance.componentInstance.cancelBtnText = conf.cancelBtnText;

    return instance.result
  }

}
