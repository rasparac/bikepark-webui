import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CollectionService {

  static update(collection: Array<any>, item: any, field: string): Array<any> {
    const newCollection = collection.slice() 
    const index = newCollection.findIndex((v) => {
      return v[field] === item[field]
    })
    if (index > -1) {
      newCollection[index] = item
    }
    return newCollection
  }

  static remove(collection: Array<any>, value: any, field: string): Array<any> {
    const newCollection = collection.slice() 
    const index = newCollection.findIndex((v) => {
      return v[field] === value
    })
    if (index > -1) {
      newCollection.splice(index, 1)
    }
    return newCollection
  }

}
