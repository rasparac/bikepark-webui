import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SpinnerService {

  private count = 0;
  private spinner = new BehaviorSubject<boolean>(false)

  constructor() { }

  spinnerAsObservable(): Observable<boolean> {
    return this.spinner.asObservable()
  }

  spin(): void {
    if (this.count == 0) {
      this.spinner.next(true);
    }
    this.count++
  }

  stop(): void {
    this.count--
    if (this.count == 0) {
      this.spinner.next(false)
    }
  }

  stopAll(): void {
    this.spinner.next(false)
  }

}
