import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { Roles, User } from '../../modules/users/interfaces/user.interface';
import { AuthUser } from '../interfaces/auth-user.interface';
import { LoginRequest, LoginResponse } from '../interfaces/auth.interface';
import { JwtService } from './jwt.service';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private currentUserSubject: Subject<AuthUser>;
  private isLogged: BehaviorSubject<boolean>
  private currentUser: AuthUser = null;

  constructor(
    private http: HttpClient,
    private cookieService: CookieService,
    private jwtService: JwtService
  ) {
    this.currentUserSubject = new Subject<AuthUser>();
    this.isLogged = new BehaviorSubject<boolean>(false);
  }

  isLoggedIn() {
    return this.isLogged.asObservable()
  }

  login(credentials: LoginRequest): Observable<LoginResponse> {
    return this.http.post<LoginResponse>("/api/v1/login", credentials);
  }

  register(user: User): Observable<User> {
    return this.http.post<User>("/api/v1/register", user)
  }

  logout() {
    this.cookieService.delete('token');
    this.currentUser = null;
    this.currentUserSubject.next(null);
    this.isLogged.next(false)
  }

  getUserObservable(): Observable<AuthUser> {
    return this.currentUserSubject.asObservable();
  }

  getUser(): AuthUser {
    if (!this.currentUser && this.cookieService.check('token')) {
      try {
        const decodedJwt = this.jwtService.parse(this.cookieService.get('token'))
        this.currentUser = <AuthUser>{
          userId: decodedJwt.user_id,
          username: decodedJwt.username,
        };
      } catch {
        return null;
      }
    }
    return this.currentUser;
  }

  setLogin(is: boolean) {
    this.isLogged.next(is);
  }

  setUser(u: AuthUser): void {
    this.currentUser = u;
    this.currentUserSubject.next(u)
  }
}
