import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BikeParkError } from '../interfaces/error.interface';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {

  constructor(
    private toastr: ToastrService
  ) { }

  success(msg: string, title?: string) {
    this.toastr.success(msg, title);
  }

  error(msg: string, title?: string) {
    this.toastr.error(msg, title);
  }

  fromError(error: HttpErrorResponse, title?: string) {
    if (error.error) {
      this.fromBikeParkErr(error.error, title)
      return
    }
    this.toastr.error(error.message, title)
  }

  fromBikeParkErr(error: BikeParkError, title?: string) {
    this.toastr.error(error.message, title)
  }

  warning(msg: string, title?: string) {
    this.toastr.warning(msg, title);
  }

}
