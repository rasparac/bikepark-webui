import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GeoLocation, GeoLocationsResponse } from '../interfaces/geo-location.interface';

@Injectable({
  providedIn: 'root'
})
export class GeoLocationService {

  errorMsg = 'An error occured. Please try again.';

  constructor(private http: HttpClient) { }

  search(address: string): Observable<GeoLocationsResponse> {
    let params = new HttpParams()
    params = params.append("filter.address", address)
    return this.http.get<GeoLocationsResponse>(this.geoSearch(), { params: params })
  }

  reverse(lat: string, lng: string): Observable<GeoLocation> {
    let params = new HttpParams()
    params = params.append("filter.latitude", lat)
    params = params.append("filter.longitude", lng)
    return this.http.get<GeoLocation>(this.geoReverse(), { params: params })
  }

  private geoSearch(): string {
    return `${this.api()}?${this.jsonFormat()}`
  }

  private geoReverse(): string {
    return `${this.api()}:reverse?${this.jsonFormat()}`
  }

  private api(): string {
    return "/api/v1/geolocations"
  }

  private jsonFormat(): string {
    return "format=json"
  }
}
