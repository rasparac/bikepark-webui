import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export abstract class ApiServiceService<T, R> {
  public url: string
  private version: string

  constructor(
    public http: HttpClient,
  ) {
    this.version = 'v1';
  }

  list(params?: any): Observable<R> {
    return this.http.get<R>(this.getFullUrl(), {params: params});
  }

  // params
  // @name resource path v1/resource/{id}
  one(name: string): Observable<T> {
    return this.http.get<T>(`/api/${this.getVersion()}/${name}`);
  }

  post(resource: T): Observable<T> {
    return this.http.post<T>(this.getFullUrl(), resource);
  }

  put(name: string, resource: T): Observable<T> {
    return this.http.put<T>(`/api/${this.getVersion()}/${name}`, resource)
  }

  delete(name: string): Observable<Object> {
    return this.http.delete(`/api/${this.getVersion()}/${name}`);
  }

  getVersion(): string {
    return this.version;
  }

  getFullUrl(): string {
    return `/api/${this.getVersion()}/${this.url}`
  }

}
