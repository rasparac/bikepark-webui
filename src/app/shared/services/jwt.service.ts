import { Injectable } from '@angular/core';

export interface DecodedJWT {
  exp: number
  iat: number
  sub: string
  username: string
  user_id: string
}

@Injectable({
  providedIn: 'root'
})
export class JwtService {

  private jwtToken: DecodedJWT;

  constructor() { }

  validate(t: DecodedJWT): boolean {
    let isValid = true;
    const now = new Date();
    const nowTs = Math.floor(now.getTime() / 1000);
    if (nowTs >= t.exp) {
      isValid = false;
    }
    return isValid;
  }

  parse(token: string): DecodedJWT {
    if (token === null || token === '') {
      throw new Error('token is null');
    }
    const parts = token.split('.');
    if (parts.length !== 3) {
      throw new Error('JWT must have 3 parts');
    }
    const decoded = this.urlBase64Decode(parts[1]);
    if (!decoded) {
      throw new Error('cannot decode the token');
    }
    this.jwtToken = JSON.parse(decoded);
    if (!this.validate(this.jwtToken)) {
      throw new Error('invalid token. token has expired.')
    }
    return this.jwtToken;
  }

  private urlBase64Decode(token: string) {
    let output = token.replace(/-/g, '+').replace(/_/g, '/');
    switch (output.length % 4) {
      case 0:
        break;
      case 2:
        output += '==';
        break;
      case 3:
        output += '=';
        break;
      default:
        throw new Error('illegal base64 string');
    }
    return decodeURIComponent(escape(atob(output)));
  }
}
