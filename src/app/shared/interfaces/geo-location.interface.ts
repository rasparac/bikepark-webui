export interface GeoLocation {
  displayName: string
  address: string
  city: string
  latitude: number
  longitude: number
}

export interface GeoLocationsResponse {
  locations: GeoLocation[]
}
