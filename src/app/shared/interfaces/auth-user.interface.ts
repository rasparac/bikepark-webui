import { UserLocation } from "../../modules/users/interfaces/user.interface"

export interface AuthUser {
  username: string
  userId: string
  userLocation: UserLocation
}
