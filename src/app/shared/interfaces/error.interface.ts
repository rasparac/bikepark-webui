export interface ErrorDetails {
  field: string
  message: string
}

export interface BikeParkError {
  details: ErrorDetails[]
  message: string
  code: number
}
