export interface Review {
    name: string
    userId: string
    placeId: string
    comment: string
    rating: number
}
  
export interface ReviewsResponse {
    reviews: Review[]
}
  