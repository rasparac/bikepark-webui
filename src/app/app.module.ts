import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { NgbActiveModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BikeParkModule } from './modules/bike-park/bike-park.module';
import { BikeTheftsModule } from './modules/bike-thefts/bike-thefts.module';
import { LocationsModule } from './modules/locations/locations.module';
import { UsersModule } from './modules/users/users.module';
import { httpInterceptorProviders } from './shared/interceptors/auth.interceptor';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    NgbModule,
    LeafletModule.forRoot(),
    ToastrModule.forRoot(),
    SharedModule,
    LocationsModule,
    UsersModule,
    BikeTheftsModule,
    BikeParkModule
  ],
  providers: [
    NgbActiveModal,
    httpInterceptorProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
