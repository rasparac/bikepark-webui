export interface UsersRespose {
  users: User[]
}

export interface User {
  name: string
  username: string
  fullName: string
  isActive: boolean
  userLocation: UserLocation
  createdAt: string
  updatedAt: string
}

export interface UserLocation {
  address: string
  latitude: number
  longitude: number
}

export enum Roles {
  USER = 'USER',
  ADMIN = 'ADMIN'
}

export interface Role {
  name: string
  displayName: string
  role: string
}
