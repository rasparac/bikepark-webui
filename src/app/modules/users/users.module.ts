import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { CreateUserComponent } from './components/create-user/create-user.component';
import { EditProfileComponent } from './components/edit-profile/edit-profile.component';
import { ListUsersComponent } from './components/list-users/list-users.component';
import { UsersRoutingModule } from './users-routing.module';


@NgModule({
  declarations: [
    CreateUserComponent,
    ListUsersComponent,
    EditProfileComponent,
  ],
  imports: [
    SharedModule,
    UsersRoutingModule
  ]
})
export class UsersModule { }
