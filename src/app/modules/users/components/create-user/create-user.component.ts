import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from 'src/app/shared/services/auth.service';
import { NotificationsService } from '../../../../shared/services/notifications.service';
import { SpinnerService } from '../../../../shared/services/spinner.service';
import { User } from '../../interfaces/user.interface';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {

  modalRef: NgbModalRef;
  userForm: FormGroup;

  constructor(
    public activeModal: NgbActiveModal,
    private authService: AuthService,
    private spinnerService: SpinnerService,
    private notificationsService: NotificationsService
  ) {
    this.userForm = new FormGroup({
      username: new FormControl(null, [Validators.required]),
      fullName: new FormControl(),
      password: new FormControl(null, [Validators.required])
    })
  }

  ngOnInit(): void {
  }

  save(): void {
    this.spinnerService.spin();
    this.authService.register(this.userForm.value).subscribe(
      r => {
        this.spinnerService.stop();
        this.activeModal.close(r);
        this.notificationsService.success("Profile has been successfully created.");
      },
      (error: HttpErrorResponse) => {
        this.spinnerService.stop();
        this.notificationsService.fromError(error);
      }
    )
  }

}
