import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { GeoLocation } from 'src/app/shared/interfaces/geo-location.interface';
import { AuthService } from 'src/app/shared/services/auth.service';
import { NotificationsService } from '../../../../shared/services/notifications.service';
import { SpinnerService } from '../../../../shared/services/spinner.service';
import { User, UserLocation } from '../../interfaces/user.interface';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {

  active = 1;
  modalRef: NgbModalRef;
  isActive: boolean;
  address: string;

  @Input() name: string;

  userForm = new FormGroup({
    username: new FormControl(),
    fullName: new FormControl()
  });

  addressForm = new FormGroup({
    address: new FormControl('', Validators.required),
    latitude: new FormControl(null),
    longitude: new FormControl(null)
  })

  constructor(
    public activeModal: NgbActiveModal,
    private usersService: UsersService,
    private notificationService: NotificationsService,
    private spinnerService: SpinnerService
  ) {
    
  }

  onSelected(location: GeoLocation): void {
    if (location.address != "") {
      this.address = location.address;
    } else {
      this.address = location.displayName;
    }

    this.addressForm.setValue({
      address: this.address,
      latitude: location.latitude,
      longitude: location.longitude
    })
  }

  ngOnInit(): void {
    this.spinnerService.spin();
    this.usersService.one(this.name).subscribe(
      r => {
        this.userForm.patchValue({
          username: r.username,
          fullName: r.fullName,
        });
        this.addressForm.patchValue({
          address: r.userLocation.address
        })

        this.address = r.userLocation.address
        this.isActive = r.isActive;
        this.spinnerService.stop();
      },
      (error: HttpErrorResponse) => {
        this.spinnerService.stop();
        this.notificationService.fromError(error);
      }
    )
  }

  update(): void {
    this.spinnerService.spin();
    if (this.active == 1) {
      const u = <User>{
        isActive: this.isActive,
        ...this.userForm.getRawValue()
      };
      this.usersService.put(this.name, u).subscribe(
        r => {
          this.activeModal.close(r);
          this.spinnerService.stop();
        },
        (error: HttpErrorResponse) => {
          this.spinnerService.stop();
          this.notificationService.fromError(error);
        }
      )
    } else if (this.active == 2) {
      if (this.addressForm.invalid) {
        return
      }

      this.usersService.editLocation(this.name, this.addressForm.value).toPromise().then(
        (_) => {
          this.activeModal.close();
        }, 
        (error: HttpErrorResponse) => {
          this.notificationService.fromError(error);
        }
      ).finally( () => {
        this.spinnerService.stop()
      })
    }
  }
}
