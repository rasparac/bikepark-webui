import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CollectionService } from '../../../../shared/services/collection.service';
import { ConfirmationService } from '../../../../shared/services/confirmation.service';
import { NotificationsService } from '../../../../shared/services/notifications.service';
import { SpinnerService } from '../../../../shared/services/spinner.service';
import { User } from '../../interfaces/user.interface';
import { UsersService } from '../../services/users.service';
import { EditProfileComponent } from '../edit-profile/edit-profile.component';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.scss']
})
export class ListUsersComponent implements OnInit {

  users: User[] = [];

  constructor(
    private modal: NgbModal,
    private usersService: UsersService,
    private spinnerService: SpinnerService,
    private confirmationService: ConfirmationService,
    private notificationsService: NotificationsService
  ) { }

  ngOnInit(): void {
    this.getUsers()
  }

  edit(name: string): void {
    const m = this.modal.open(EditProfileComponent, {
      centered: true,
      size: 'lg'
    });
    m.componentInstance.name = name;
    m.result.then(user => {
      if (user) {
        this.users = CollectionService.update(this.users, user, "name");
        this.notificationsService.success("User successfully updated!");
      }
    }, _ => { });
  }

  delete(name: string): void {
    this.confirmationService.confirm().then(yes => {
      if (yes) {
        this.usersService.delete(name).subscribe(
          r => {
            this.notificationsService.success("User successfully deleted!");
          },
          (error: HttpErrorResponse) => {
            this.notificationsService.fromError(error);
          }
        )
      }
    }, _ => { });
  }

  private getUsers(): void {
    this.spinnerService.spin();
    this.usersService.list().subscribe(
      r => {
        this.users = r.users;
        this.spinnerService.stop();
      },
      (error: HttpErrorResponse) => {
        this.spinnerService.stop();
        this.notificationsService.fromError(error);
      }
    )
  }

}
