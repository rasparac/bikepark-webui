import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EditBikeParkComponent } from '../../../../modules/bike-park/components/edit-bike-park/edit-bike-park.component';
import { LocaitonType, Location } from '../../../../modules/locations/interfaces/location.inteface';
import { MapViewLocationComponent } from '../../../../shared/components/map-view-location/map-view-location.component';
import { CollectionService } from '../../../../shared/services/collection.service';
import { ConfirmationService } from '../../../../shared/services/confirmation.service';
import { NotificationsService } from '../../../../shared/services/notifications.service';
import { SpinnerService } from '../../../../shared/services/spinner.service';
import { BikeTheft } from '../../interfaces/bike-theft.interface';
import { BikeTheftsService } from '../../services/bike-thefts.service';
import { EditBikeTheftComponent } from '../edit-bike-theft/edit-bike-theft.component';

@Component({
  selector: 'app-my-bike-thefts',
  templateUrl: './my-bike-thefts.component.html',
  styleUrls: ['./my-bike-thefts.component.scss']
})
export class MyBikeTheftsComponent implements OnInit {

  bikeThefts: BikeTheft[] = [];

  constructor(
    private modalService: NgbModal,
    private confirmationService: ConfirmationService,
    private notificationsService: NotificationsService,
    private bikeTheftsService: BikeTheftsService,
    private spinnerService: SpinnerService
  ) { }

  ngOnInit(): void {
    this.bikeTheftsService.myBikeThefts().subscribe(
      r => {
        this.bikeThefts = r.bikeThefts;
      },
      (error: HttpErrorResponse) => {
        this.notificationsService.fromError(error);
      }
    );
  }

  edit(theft: BikeTheft) {
    const m = this.modalService.open(EditBikeTheftComponent, {
      size: 'lg',
      keyboard: false
    });
    m.componentInstance.bikeTheft = theft;
    m.result.then(result => {
      if (result) {
        this.bikeThefts = CollectionService.update(this.bikeThefts, result, "name");
      }
    }, _ => { });
  }

  delete(name: string) {
    this.confirmationService.confirm().then(response => {
      if (response) {
        this.spinnerService.spin();
        this.bikeTheftsService.delete(name).subscribe(
          _ => {
            this.notificationsService.success("Location successfully deleted.");
            this.bikeThefts = CollectionService.remove(this.bikeThefts, name, "name");
            this.spinnerService.stop();
          },
          (error: HttpErrorResponse) => {
            this.spinnerService.stop();
            this.notificationsService.fromError(error);
          }
        )
      }
    }, _ => { });
  }

  mapView(bt: BikeTheft) {
    const m = this.modalService.open(MapViewLocationComponent, {
      size: 'lg',
      keyboard: false,
      centered: true
    });
    let loc = <Location>{
      city: bt.city,
      description: bt.description,
      latitude: bt.latitude,
      longitude: bt.longitude,
      street: bt.street,
      type: LocaitonType.Theft,
      user: bt.user
    }
    m.componentInstance.location = loc;
  }

}
