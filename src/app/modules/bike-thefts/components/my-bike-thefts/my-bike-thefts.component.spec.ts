import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyBikeTheftsComponent } from './my-bike-thefts.component';

describe('MyBikeTheftsComponent', () => {
  let component: MyBikeTheftsComponent;
  let fixture: ComponentFixture<MyBikeTheftsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyBikeTheftsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyBikeTheftsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
