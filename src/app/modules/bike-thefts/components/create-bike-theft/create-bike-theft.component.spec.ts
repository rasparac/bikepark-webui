import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateBikeTheftComponent } from './create-bike-theft.component';

describe('CreateBikeTheftComponent', () => {
  let component: CreateBikeTheftComponent;
  let fixture: ComponentFixture<CreateBikeTheftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateBikeTheftComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateBikeTheftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
