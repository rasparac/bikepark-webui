import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiServiceService } from '../../../shared/services/api-service.service';
import { BikeTheft, BikeTheftResponse } from '../interfaces/bike-theft.interface';

export interface NearstQuery {
  latitude: number
  longitude: number
  limit: number,
  distance: number
}

@Injectable({
  providedIn: 'root'
})
export class BikeTheftsService extends ApiServiceService<BikeTheft, BikeTheftResponse> {

  constructor(public http: HttpClient) {
    super(http)
    this.url = "bikethefts";
  }

  myBikeThefts(): Observable<BikeTheftResponse> {
    return this.http.get<BikeTheftResponse>(`${this.getFullUrl()}:myBikeThefts`);
  }

  nearest(query: NearstQuery): Observable<BikeTheftResponse> {
    const p = new HttpParams({
      fromObject: {
        "filter.latitude": query.latitude.toString(),
        "filter.longitude": query.longitude.toString(),
        "filter.distance": query.distance.toString(),
        "limit": query.limit.toString()
      }
    })
    return this.http.get<BikeTheftResponse>(`${this.getFullUrl()}:nearest`, { params: p });
  }
}
