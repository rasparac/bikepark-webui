import { User } from "../../users/interfaces/user.interface";

export interface BikeTheftResponse {
  bikeThefts: BikeTheft[];
}

export interface BikeTheft {
  name: string
  city: string
  street: string
  description: string
  latitude: number
  longitude: number
  createdAt: string
  updatedAt: string
  deletedAt: string
  isFound: boolean
  user: User
}
