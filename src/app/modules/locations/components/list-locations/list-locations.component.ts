import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit, SimpleChange } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BikeParkingsService } from '../../../../modules/bike-park/services/bike-parkings.service';
import { EditBikeTheftComponent } from '../../../../modules/bike-thefts/components/edit-bike-theft/edit-bike-theft.component';
import { Roles } from '../../../../modules/users/interfaces/user.interface';
import { MapViewLocationComponent } from '../../../../shared/components/map-view-location/map-view-location.component';
import { AuthService } from '../../../../shared/services/auth.service';
import { CollectionService } from '../../../../shared/services/collection.service';
import { ConfirmationService } from '../../../../shared/services/confirmation.service';
import { NotificationsService } from '../../../../shared/services/notifications.service';
import { SpinnerService } from '../../../../shared/services/spinner.service';
import { Location } from '../../interfaces/location.inteface';

@Component({
  selector: 'app-list-locations',
  templateUrl: './list-locations.component.html',
  styleUrls: ['./list-locations.component.scss']
})
export class ListLocationsComponent implements OnInit {

  @Input() locations: Location[];
  @Input() showActions = false;

  constructor(
    private modalService: NgbModal,
    private confirmationService: ConfirmationService,
    private notificationsService: NotificationsService,
    private spinnerService: SpinnerService,
    private bikeParkingsService: BikeParkingsService
  ) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: { [key: string]: SimpleChange }) {
    if (changes["locations"]) {
      this.locations = changes["locations"].currentValue;
    }
  }

  mapView(l: Location): void {
    const m = this.modalService.open(MapViewLocationComponent, {
      size: 'lg',
      keyboard: false,
      centered: true
    });
    m.componentInstance.location = l;
  }

  edit(loc: Location): void {
    const m = this.modalService.open(EditBikeTheftComponent, {
      size: 'lg',
      keyboard: false
    });
    m.componentInstance.location = loc;
    m.result.then(result => {
      if (result) {
        this.locations = CollectionService.update(this.locations, result, "name");
      }
    }, _ => { });
  }

  delete(name: string): void {
    this.confirmationService.confirm().then(response => {
      if (response) {
        this.spinnerService.spin();
        this.bikeParkingsService.delete(name).subscribe(
          _ => {
            this.notificationsService.success("Location successfully deleted.");
            this.locations = CollectionService.remove(this.locations, name, "name");
            this.spinnerService.stop();
          },
          (error: HttpErrorResponse) => {
            this.spinnerService.stop();
            this.notificationsService.fromError(error);
          }
        )
      }
    }, _ => { });
  }

}
