import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BikeTheft } from 'src/app/modules/bike-thefts/interfaces/bike-theft.interface';
import { BikeTheftsService } from 'src/app/modules/bike-thefts/services/bike-thefts.service';
import { BikeParking } from '../../../../modules/bike-park/interfaces/bike-parking.interface';
import { BikeParkingsService } from '../../../../modules/bike-park/services/bike-parkings.service';
import { GeoLocation } from '../../../../shared/interfaces/geo-location.interface';
import { AuthService } from '../../../../shared/services/auth.service';
import { LatLong, NavigatorService } from '../../../../shared/services/navigator.service';
import { NotificationsService } from '../../../../shared/services/notifications.service';
import { SpinnerService } from '../../../../shared/services/spinner.service';
import { CreateBikeParkComponent } from '../../../bike-park/components/create-bike-park/create-bike-park.component';
import { CreateBikeTheftComponent } from '../../../bike-thefts/components/create-bike-theft/create-bike-theft.component';
import { LocaitonType, Location } from '../../interfaces/location.inteface';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.scss']
})
export class LocationsComponent implements OnInit {

  isListView = false;
  isLoggedIn = false;
  showThefts = false;
  isLocationActive = false;
  selectedGeoLocation: GeoLocation;
  view = [];
  locations: Location[] = [];
  latitude: number;
  longitude: number;

  constructor(
    private bikeParkingsService: BikeParkingsService,
    private bikeTheftsService: BikeTheftsService,
    private spinnerService: SpinnerService,
    private modalService: NgbModal,
    private authService: AuthService,
    private notificationsService: NotificationsService,
    private navigator: NavigatorService
  ) { }

  ngOnInit(): void {
    this.authService.isLoggedIn().subscribe(is => {
      this.isLoggedIn = is;
      if (is) {
        const userLocation = this.authService.getUser().userLocation
        if (userLocation && userLocation.address != "") {
          this.setView([userLocation.latitude, userLocation.longitude]);
        }
      }
    });
  }

  useMyLocation() {
    this.navigator.currentPosition().then(
      (latLong) => {
        this.isLocationActive = true;
        this.latitude = latLong[0];
        this.longitude = latLong[1];
        this.setView(latLong);
        this.getAndSetLocations([latLong[0], latLong[1]]);
      },
      (error) => {
        this.notificationsService.warning(error, "GeoLocation")
      }
    );
  }

  onSelected(gl: GeoLocation) {
    let latitude: number;
    let longitude: number;
    let searchForLocations = false;
    if (this.isLocationActive) {
      latitude = this.latitude;
      longitude = this.longitude;
      searchForLocations = true;
    }
    if (gl) {
      latitude = gl.latitude;
      longitude = gl.longitude;
      searchForLocations = true;
    }
    this.locations = [];
    if (searchForLocations) {
      this.setView([latitude, longitude]);
      this.getAndSetLocations([latitude, longitude]);
    }
  }

  addBikeParking(): void {
    const m = this.modalService.open(CreateBikeParkComponent, {
      size: 'lg',
      centered: true
    })
    m.result.then(value => {
      if (value) {
        if (this.isLocationActive) {
          this.getAndSetLocations([this.latitude, this.longitude]);
        }
      }
    }, _ => { })
  }

  addBikeTheft(): void {
    const m = this.modalService.open(CreateBikeTheftComponent, {
      size: 'lg',
      centered: true
    })
    m.result.then(value => {
      if (value) {
        if (this.isLocationActive) {
          this.getAndSetLocations([this.latitude, this.longitude]);
        }
      }
    }, _ => { })
  }

  onEnter(gl: GeoLocation): void {
    if (gl) {
      this.setView([gl.latitude, gl.longitude]);
      this.getAndSetLocations([gl.latitude, gl.longitude]);
    }
  }

  onMoveEnd(value: [number, number]) {
    this.latitude = value[0];
    this.longitude = value[1];
  }

  showBikeThefts(): void {
    if (!this.latitude || !this.longitude) {
      return
    }

    this.spinnerService.spin();
    this.bikeTheftsService.nearest({
      distance: 1,
      latitude: this.latitude,
      longitude: this.longitude,
      limit: 20
    }).subscribe(
      r => {
        const loc = this.locations.filter(x => x.type == LocaitonType.Parking);
      
        r.bikeThefts.forEach(bt => {
          loc.push(this.bikeTheftToLocation(bt))
        })
      
        this.locations = loc.slice()
        this.spinnerService.stop();
      },
      error => {
        this.spinnerService.stop();
        this.notificationsService.fromError(error);
      }
    )
  }

  searchArea(): void {
    this.getAndSetLocations([this.latitude, this.longitude]);
  }

  toggleLocatioView(): void {
    this.isListView = !this.isListView;
  }

  private getAndSetLocations(ll: LatLong): void {
    this.spinnerService.spin();
    this.bikeParkingsService.nearest({
      latitude: ll[0],
      longitude: ll[1],
      distance: 1, // km
      limit: 20
    }).subscribe(
      r => {
        const loc: Location[] = [];
        r.bikeParkings.forEach(bp => {
          loc.push(this.bikeParkingToLocation(bp))
        });
        this.locations = loc;
        this.spinnerService.stop();
      },
      (error: HttpErrorResponse) => {
        this.spinnerService.stop();
        this.notificationsService.fromError(error);
      }
    )
  }

  private setView(ll: LatLong): void {
    this.view = [ll[0], ll[1]];
  }

  private bikeParkingToLocation(bp: BikeParking): Location {
    let loc = <Location>{
      name: bp.name,
      city: bp.city,
      description: bp.description,
      isSuggestion: bp.isSuggestion,
      latitude: bp.latitude,
      longitude: bp.longitude,
      street: bp.street,
      type: LocaitonType.Parking,
      user: bp.user
    }
    return loc
  }

  private bikeTheftToLocation(bt: BikeTheft): Location {
    let loc = <Location>{
      name: bt.name,
      city: bt.city,
      description: bt.description,
      latitude: bt.latitude,
      longitude: bt.longitude,
      street: bt.street,
      type: LocaitonType.Theft,
      user: bt.user
    }
    return loc
  }

}
