import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LeafletDirective, LeafletModule } from '@asymmetrik/ngx-leaflet';
import { SharedModule } from '../../shared/shared.module';
import { ListLocationsComponent } from './components/list-locations/list-locations.component';
import { LocationsMapComponent } from './components/locations-map/locations-map.component';
import { LocationsComponent } from './components/locations/locations.component';
import { ViewLocationComponent } from './components/view-location/view-location.component';


@NgModule({
  declarations: [
    ViewLocationComponent,
    LocationsComponent,
    LocationsMapComponent,
    ListLocationsComponent
  ],
  providers: [
    LeafletDirective
  ],
  imports: [
    CommonModule,
    LeafletModule,
    SharedModule
  ]
})
export class LocationsModule { }
