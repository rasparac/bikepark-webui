import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyBikeParkingsComponent } from './components/my-bike-parkings/my-bike-parkings.component';


const routes: Routes = [
  {
    path: "my-bikeparkings",
    component: MyBikeParkingsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BikeParkRoutingModule { }
