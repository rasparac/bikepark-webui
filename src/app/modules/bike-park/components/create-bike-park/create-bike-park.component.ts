import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { latLng, LatLng, Map, MapOptions, Marker, marker, MarkerOptions, tileLayer } from 'leaflet';
import { GeoLocation } from '../../../../shared/interfaces/geo-location.interface';
import { GeoLocationService } from '../../../../shared/services/geo-location.service';
import { bikeParkingIconPath, LeafletService } from '../../../../shared/services/leaflet.service';
import { NotificationsService } from '../../../../shared/services/notifications.service';
import { SpinnerService } from '../../../../shared/services/spinner.service';
import { BikeParkingsService } from '../../services/bike-parkings.service';

@Component({
  selector: 'app-create-bike-park',
  templateUrl: './create-bike-park.component.html',
  styleUrls: ['./create-bike-park.component.scss']
})
export class CreateBikeParkComponent implements OnInit {

  locationForm: FormGroup
  options: any
  map: Map
  lastMarker: Marker

  constructor(
    public activeModal: NgbActiveModal,
    private bikeParkingsService: BikeParkingsService,
    private geoSearchService: GeoLocationService,
    private notificationsService: NotificationsService,
    private spinnerService: SpinnerService,
  ) {
    this.locationForm = new FormGroup({
      city: new FormControl(null, [Validators.required]),
      street: new FormControl(null, [Validators.required]),
      latitude: new FormControl(null, [Validators.required]),
      longitude: new FormControl(null, [Validators.required]),
      description: new FormControl({ value: null, disabled: false }),
      isSuggestion: new FormControl({ value: null, disabled: false })
    })
  }

  ngOnInit(): void {
    this.options = <MapOptions>{
      layers: [
        tileLayer(LeafletService.URL, { maxZoom: 18, attribution: 'Open Street Map' }),
      ],
      zoom: 12,
      scrollWheelZoom: false,
      center: latLng(45.8103223, 15.9795587)
    }
  }

  onSelected(gl: GeoLocation) {
    if (gl) {
      if (this.lastMarker) {
        this.lastMarker.remove()
      }
      const foundLocation = gl;
      this.locationForm.patchValue({
        "city": foundLocation.city,
        "street": foundLocation.address,
        "latitude": foundLocation.latitude,
        "longitude": foundLocation.longitude,
      })
    
      const latLngE = latLng(foundLocation.latitude, foundLocation.longitude)
      this.map.setView(latLngE, 16)
      const marker = this.createMarkerFromLocation(latLngE)
      marker.on('dragend', e => {
        this.spinnerService.spin();
        const target = e.target
        const lat = target.getLatLng().lat
        const lng = target.getLatLng().lng
        this.geoSearchService.reverse(lat, lng).subscribe(
          r => {
            this.locationForm.patchValue({
              "city": r.city,
              "street": r.address,
              "latitude": r.latitude,
              "longitude": r.longitude,
            })
            this.spinnerService.stop();
          },
          e => {
            console.error("OpenStreetMap: ", e);
            this.spinnerService.stop();
            this.notificationsService.error(this.geoSearchService.errorMsg);
          }
        )
      })
      marker.addTo(this.map)
      this.lastMarker = marker
    }
  }

  onMapReady(map: Map) {
    this.map = map
  }

  close(l?: Location): void {
    this.activeModal.close(l)
  }

  save() {
    this.spinnerService.spin();
    let loc = {
      ...this.locationForm.getRawValue()
    }
    this.bikeParkingsService.post(loc).subscribe(
      r => {
        this.close(loc)
        this.spinnerService.stop();
        this.notificationsService.success(this.bikeParkingsService.createdMessage)
      },
      (error: HttpErrorResponse) => {
        this.notificationsService.fromError(error);
        this.spinnerService.stop();
      }
    )
  }

  private createMarkerFromLocation(latLng: LatLng): Marker {
    const options: MarkerOptions = {
      draggable: true,
      icon: LeafletService.icon(bikeParkingIconPath)
    }
    return marker(latLng, options)
  }

}
