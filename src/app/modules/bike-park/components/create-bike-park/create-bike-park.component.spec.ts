import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateBikeParkComponent } from './create-bike-park.component';

describe('CreateBikeParkComponent', () => {
  let component: CreateBikeParkComponent;
  let fixture: ComponentFixture<CreateBikeParkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateBikeParkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateBikeParkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
