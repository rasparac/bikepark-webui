import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditBikeParkComponent } from './edit-bike-park.component';

describe('EditBikeParkComponent', () => {
  let component: EditBikeParkComponent;
  let fixture: ComponentFixture<EditBikeParkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditBikeParkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditBikeParkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
