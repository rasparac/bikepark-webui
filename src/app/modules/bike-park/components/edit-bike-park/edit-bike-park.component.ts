import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { latLng, LatLng, Map, MapOptions, marker, Marker, MarkerOptions, tileLayer } from 'leaflet';
import { GeoLocation } from '../../../../shared/interfaces/geo-location.interface';
import { GeoLocationService } from '../../../../shared/services/geo-location.service';
import { bikeParkingIconPath, LeafletService } from '../../../../shared/services/leaflet.service';
import { NotificationsService } from '../../../../shared/services/notifications.service';
import { SpinnerService } from '../../../../shared/services/spinner.service';
import { BikeParking } from '../../interfaces/bike-parking.interface';
import { BikeParkingsService } from '../../services/bike-parkings.service';

@Component({
  selector: 'app-edit-bike-park',
  templateUrl: './edit-bike-park.component.html',
  styleUrls: ['./edit-bike-park.component.scss']
})
export class EditBikeParkComponent implements OnInit {

  bikeParkForm: FormGroup
  options: any
  map: Map
  lastMarker: Marker
  marker: Marker

  @Input() location: BikeParking;

  constructor(
    public activeModal: NgbActiveModal,
    private bikeParkingService: BikeParkingsService,
    private geoSearchService: GeoLocationService,
    private notificationsService: NotificationsService,
    private spinnerService: SpinnerService,
  ) {
    this.bikeParkForm = new FormGroup({
      city: new FormControl(),
      street: new FormControl(),
      latitude: new FormControl({ value: '', disabled: true }),
      longitude: new FormControl({ value: '', disabled: true }),
      description: new FormControl(),
      isSuggestion: new FormControl()
    })
  }

  ngOnInit(): void {
    this.options = <MapOptions>{
      layers: [
        tileLayer(LeafletService.URL, { maxZoom: 18, attribution: 'Open Street Map' }),
      ],
      zoom: 12,
      scrollWheelZoom: false,
    }

    this.bikeParkForm.patchValue(<BikeParking>{
      city: this.location.city,
      street: this.location.street,
      latitude: this.location.latitude,
      longitude: this.location.longitude,
      description: this.location.description,
      isSuggestion: this.location.isSuggestion
    })
  }

  onSelected(gl: GeoLocation) {
    if (gl) {
      if (this.lastMarker) {
        this.lastMarker.remove()
      }
      const foundLocation = gl;
      this.bikeParkForm.patchValue({
        "city": foundLocation.city,
        "street": foundLocation.address,
        "latitude": foundLocation.latitude,
        "longitude": foundLocation.longitude,
      })
      const latLngE = latLng(foundLocation.latitude, foundLocation.longitude)
      this.map.setView(latLngE, 16)
      const marker = this.createMarkerFromLocation(latLngE)
      marker.on('dragend', e => {
        this.spinnerService.spin();
        const target = e.target
        const lat = target.getLatLng().lat
        const lng = target.getLatLng().lng
        this.geoSearchService.reverse(lat, lng).subscribe(
          r => {
            this.bikeParkForm.patchValue({
              "city": r.city,
              "street": r.address,
              "latitude": r.latitude,
              "longitude": r.longitude,
            })
            this.spinnerService.stop();
          },
          _ => {
            this.spinnerService.stop();
            this.notificationsService.error(this.geoSearchService.errorMsg);
          }
        )
      })
      marker.addTo(this.map)
      this.lastMarker = marker
    }
  }

  onMapReady(map: Map) {
    this.map = map
    const latLngE = latLng(this.location.latitude, this.location.longitude)
    this.marker = this.setMarker(latLngE)
    this.marker.addTo(this.map)
    setTimeout(() => {
      this.map.setView(latLngE, 16)
    }, 500)
  }

  close(loc?: BikeParking): void {
    this.activeModal.close(loc)
  }

  save() {
    let loc: BikeParking = {
      ...this.bikeParkForm.getRawValue()
    }
    this.spinnerService.spin();
    this.bikeParkingService.put(this.location.name, loc).subscribe(
      resp => {
        this.notificationsService.success(this.bikeParkingService.editMessage)
        this.close(resp)
        this.spinnerService.stop();
      },
      (error: HttpErrorResponse) => {
        this.close();
        this.spinnerService.stop();
        this.notificationsService.fromError(error);
      }
    )
  }

  private setMarker(latLng: LatLng): Marker {
    const m = this.createMarkerFromLocation(latLng)
    m.on('dragend', e => {
      this.spinnerService.spin();
      const target = e.target;
      const lat = target.getLatLng().lat;
      const lng = target.getLatLng().lng;
      this.geoSearchService.reverse(lat, lng).subscribe(
        r => {
          this.bikeParkForm.patchValue({
            "city": r.city,
            "street": r.address,
            "latitude": r.latitude,
            "longitude": r.longitude,
          })
          this.spinnerService.stop();
        },
        (error: HttpErrorResponse) => {
          this.spinnerService.stop();
          this.notificationsService.fromError(error);
        }
      )
    })
    return m
  }

  private createMarkerFromLocation(latLng: LatLng): Marker {
    const options: MarkerOptions = {
      draggable: true,
      icon: LeafletService.icon(bikeParkingIconPath)
    }
    return marker(latLng, options)
  }


}
