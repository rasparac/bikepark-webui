import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LocaitonType, Location } from '../../../../modules/locations/interfaces/location.inteface';
import { MapViewLocationComponent } from '../../../../shared/components/map-view-location/map-view-location.component';
import { CollectionService } from '../../../../shared/services/collection.service';
import { ConfirmationService } from '../../../../shared/services/confirmation.service';
import { NotificationsService } from '../../../../shared/services/notifications.service';
import { SpinnerService } from '../../../../shared/services/spinner.service';
import { BikeParking } from '../../interfaces/bike-parking.interface';
import { BikeParkingsService } from '../../services/bike-parkings.service';
import { EditBikeParkComponent } from '../edit-bike-park/edit-bike-park.component';

@Component({
  selector: 'app-my-bike-parkings',
  templateUrl: './my-bike-parkings.component.html',
  styleUrls: ['./my-bike-parkings.component.scss']
})
export class MyBikeParkingsComponent implements OnInit {

  parkings: BikeParking[] = [];

  constructor(
    private bikeParkingService: BikeParkingsService,
    private notificationsService: NotificationsService,
    private confirmationService: ConfirmationService,
    private spinnerService: SpinnerService,
    private modalService: NgbModal
  ) { }

  ngOnInit(): void {
    this.bikeParkingService.myBikeParkings().subscribe(
      r => {
        this.parkings = r.bikeParkings;
      },
      (error: HttpErrorResponse) => {
        this.notificationsService.fromError(error);
      }
    );
  }

  edit(bp: BikeParking) {
    const m = this.modalService.open(EditBikeParkComponent, {
      size: 'lg',
      keyboard: false
    });
    m.componentInstance.location = bp;
    m.result.then(result => {
      if (result) {
        this.parkings = CollectionService.update(this.parkings, result, "name");
      }
    }, _ => { });
  }

  delete(name: string) {
    this.confirmationService.confirm().then(response => {
      if (response) {
        this.spinnerService.spin();
        this.bikeParkingService.delete(name).subscribe(
          _ => {
            this.notificationsService.success("Bike Park successfully deleted.");
            this.parkings = CollectionService.remove(this.parkings, name, "name");
            this.spinnerService.stop();
          },
          (error: HttpErrorResponse) => {
            this.spinnerService.stop();
            this.notificationsService.fromError(error);
          }
        )
      }
    }, _ => { });
  }

  mapView(bp: BikeParking) {
    const m = this.modalService.open(MapViewLocationComponent, {
      size: 'lg',
      keyboard: false,
      centered: true
    });
    let loc = <Location>{
      city: bp.city,
      description: bp.description,
      latitude: bp.latitude,
      longitude: bp.longitude,
      street: bp.street,
      type: LocaitonType.Parking,
      user: bp.user
    }
    m.componentInstance.location = loc;
  }

}
