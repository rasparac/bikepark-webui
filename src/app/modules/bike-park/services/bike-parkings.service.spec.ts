import { TestBed } from '@angular/core/testing';

import { BikeParkingsService } from './bike-parkings.service';

describe('BikeParkingsService', () => {
  let service: BikeParkingsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BikeParkingsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
