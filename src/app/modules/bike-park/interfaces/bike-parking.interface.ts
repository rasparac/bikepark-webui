import { User } from '../../users/interfaces/user.interface';

export interface BikeParkingsResponse {
  bikeParkings: BikeParking[]
}

export interface BikeParking {
  name: string
  city: string
  street: string
  description: string
  isSuggestion: boolean
  latitude: number
  longitude: number
  createdAt: string
  updatedAt: string
  deletedAt: string
  user: User
}
